gcc -c mandelbrot.c -o mandelbrot.o -fopenmp -lm
gcc mandelbrot.o -o mandelbrot -fopenmp -lm
./mandelbrot
rm mandelbrot.o
mv mandelbrot.ppm mandelbrotNormal.ppm
