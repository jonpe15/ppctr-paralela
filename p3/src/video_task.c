#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

void fgauss (int *, int *, int, int);

int main(int argc, char *argv[]) {

	printf("---------------------------------\n");
	printf("Empezamos PARALELO\n");
	printf("---------------------------------\n");
	FILE *in;
	FILE *out;
	int i, j, size, cont, seq = 4;
	int **pixels, **filtered;

	if (argc == 2) seq = atoi (argv[1]);

	//   chdir("/tmp");
	in = fopen("movie.in", "rb");
	if (in == NULL) {
	  perror("movie.in");
	  exit(EXIT_FAILURE);
	}

	out = fopen("movie.out", "wb");
	if (out == NULL) {
	  perror("movie.out");
	  exit(EXIT_FAILURE);
	}

	int width, height;

	fread(&width, sizeof(width), 1, in);
	fread(&height, sizeof(height), 1, in);

	fwrite(&width, sizeof(width), 1, out);
	fwrite(&height, sizeof(height), 1, out);

	pixels = (int **) malloc (seq * sizeof (int *));
	filtered = (int **) malloc (seq * sizeof (int *));

	for (i=0; i<seq; i++)
	{
		pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
		filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
	}
	
	double start = omp_get_wtime();
	#pragma omp parallel shared(size,pixels,height,width,in,filtered,out) private(i)
	{
		#pragma omp single 
		{
			
			do
			{
				cont=0;
				for(i=0; i<seq; i++){
					size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);
					printf("i= %d\n", i);
					if (size) //se ha leído correctamente, !=0
					{
						printf("Imagen %d leída correctamente\n", cont);
						cont++;
					}
				}
			
				for (i=0; i<cont; i++){
					#pragma omp task
					{
					fgauss (pixels[i], filtered[i], height, width);
					}
					printf("Tarea creada para la imagen %d\n", i);
				}
				#pragma omp taskwait
				for (i=0; i<cont; i++){
					fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
					printf("Imagen filtrada número %d escrita\n", i);
				} 
				i=0;
			} while (!feof(in));
		}//end single
	}
	
	double end = omp_get_wtime();
	printf("El programa ha tardado %f segundos\n", end-start);

	for (i=0; i<seq; i++)
	{
		free (pixels[i]);
		free (filtered[i]);
	}
	free(pixels);
	free(filtered);

	fclose(out);
	fclose(in);

	return EXIT_SUCCESS;
}

void fgauss (int *pixels, int *filtered, int heigh, int width)
{
	int y, x, dx, dy;
	int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
	int sum;

	printf("Entramos en fgauss\n");
	double start_fgauss = omp_get_wtime();
	//#pragma omp parallel shared (width,heigh,sum,pixels,filter) private (x,y,dx,dy)
	//{
		//#pragma omp for collapse(2)
		for (x = 0; x < width; x++) {
			for (y = 0; y < heigh; y++)
			{	
				sum = 0;
				for (dx = 0; dx < 5; dx++)
					for (dy = 0; dy < 5; dy++)
						if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < heigh))
							//#pragma omp atomic							
							sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
				filtered[x*heigh+y] = (int) sum/273;
			}
		}
	//}
	printf("Salimos de fgauss\n");
	double end_fgauss = omp_get_wtime();
	printf("Tiempo en fgauss: %f segundos\n", end_fgauss-start_fgauss);
}
