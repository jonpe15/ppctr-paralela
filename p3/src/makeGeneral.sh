gcc -c generator.c -o generator.o -fopenmp
gcc generator.o -o generator -fopenmp
./generator
rm generator.o
gcc -c video_task.c -o video_task.o -fopenmp
gcc video_task.o -o video_task -fopenmp
./video_task
rm video_task.o
mv movie.out moviePar.out
gcc -c video_taskNP.c -o video_taskNP.o -fopenmp
gcc video_taskNP.o -o video_taskNP -fopenmp
./video_taskNP
rm video_taskNP.o
mv movie.out movieSec.out
