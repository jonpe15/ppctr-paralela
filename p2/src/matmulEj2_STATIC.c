#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>

#define RAND rand() % 100

void init_mat_sup ();
void init_mat_inf ();
void matmul ();
void matmul_sup ();
void matmul_inf ();
void print_matrix (float *M, int dim); // TODO

/* Usage: ./matmul <dim> [block_size]*/
void print_matrix (float *M, int dim)
{
	int i, j;

	for (i=0; i < dim; i++)
	{
		for (j=0; j < dim; j++)
			fprintf (stdout, "%.1f ", M[i*dim+j]);
		fprintf (stdout,"\n");
	}

   printf ("\n");
}

int main (int argc, char* argv[])
{
	int block_size = 1, dim;
	float *A, *B, *C;


	dim = atoi (argv[1]);
	if (argc == 3) block_size = atoi (argv[2]); 

	A = (float *) malloc (dim * dim * sizeof (float));
	B = (float *) malloc (dim * dim * sizeof (float));
	C = (float *) malloc (dim * dim * sizeof (float));

	init_mat_sup (dim, A);
	init_mat_inf (dim, B);
	printf ("Matriz A:\n");
	print_matrix (A, dim);
	printf ("Matriz B:\n");
	print_matrix (B, dim);

	matmul (A, B, C, dim);
	printf ("Resultado matmul:\n");
	print_matrix (C, dim);

	printf ("matmul SUP\n");
	matmul_sup (A, B, C, dim);
	print_matrix (C, dim);

	init_mat_inf (dim, A);
	init_mat_sup (dim, B);

	printf ("Matriz A\n");
	print_matrix (A, dim);
	printf ("Matriz B\n");
	print_matrix (B, dim);

	matmul (A, B, C, dim);
	printf ("Resultado matmul\n");
	print_matrix (C, dim);

	printf ("matmul INF\n");
	matmul_inf (A, B, C, dim);
	print_matrix (C, dim);
	
	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j;
	double start = omp_get_wtime();


	//no paralelizo estos bucles porque tardan más tiempo paralelizados que sin paralelizar
	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
	
	double end = omp_get_wtime();
	printf("El programa ha tardado %f segundos en inicializar\n", end-start);
}

void init_mat_inf (int dim, float *M)
{
	int i,j;
	double start = omp_get_wtime();

	
	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
				M[i*dim+j] = 0.0;
			else
				M[i*dim+j] = RAND;
		}
	}
	
	double end = omp_get_wtime();
	printf("El programa ha tardado %f segundos en inicializar\n", end-start);
}

void matmul (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	double start = omp_get_wtime();

	
	//no paralelizo este bucle porque necesito un valor consistente de C al salir de él
	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;
	#pragma omp parallel private (i,j,k) shared (A,B,C,dim)
	{
		#pragma omp for
		for (i=0; i < dim; i++)
			for (j=0; j < dim; j++)
				for (k=0; k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];//no es crítico porque nunca van a acceder a la misma posición en el mismo momento
	}
	double end = omp_get_wtime();
	printf("El programa ha tardado %f segundos en multiplicar las matrices\n", end-start);
} 

void matmul_sup (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	double start = omp_get_wtime();

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;
	#pragma omp parallel private (i,j,k) shared (A,B,C,dim)
	{
		#pragma omp for schedule(static)
		for (i=0; i < (dim-1); i++)
			for (j=0; j < (dim-1); j++)
				for (k=(i+1); k < dim; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
	double end = omp_get_wtime();
	printf("El programa ha tardado %f segundos en multiplicar las matrices SUP\n", end-start);
} 

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;
	double start = omp_get_wtime();

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;
	#pragma omp parallel private (i,j,k) shared (A,B,C,dim)
	{
		#pragma omp for
		for (i=1; i < dim; i++)
			for (j=1; j < dim; j++)
				for (k=0; k < i; k++)
					C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
	double end = omp_get_wtime();
	printf("El programa ha tardado %f segundos en multiplicar las matrices INF\n", end-start);
} 

